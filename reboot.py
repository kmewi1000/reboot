#!/usr/bin/env python3

import sys
from gi.repository import Gtk

class ask_confirm:
	def run(self):
		self.interfaz =Gtk.Builder()
		self.interfaz.add_from_file("ask.ui") #carga archivo glade que contiene la interfaz grafica
		self.ventana=self.interfaz.get_object("dialog1") #carga el objeto ventana de dialogo
		self.ventana.show_all()
		
		retval=self.ventana.run() #creamos la variable retval con el evento de correr ventana para permitir que continue funcionando la pila de eventos
		'''cerramos la ventana de dialogo y vaciamos la variable ventana'''
		self.ventana.hide()
		self.ventana.destroy()
		self.ventana=None
		
		''' 1 = cancelar apagado; 2 = aceptar apagado'''  
		
		if(retval==2):
			return True
		else:
			return False
			

class main_window:
	def __init__(self):
		self.interfaz =Gtk.Builder()
		self.interfaz.add_from_file("reboot.ui") #carga archivo glade que contiene la interfaz grafica
		self.interfaz.connect_signals(self) #conecta senhales del glade
		self.window=self.interfaz.get_object("window1")
		self.window.show()
		
		''' FUNCION DE APAGADO'''
		
	def on_button_apagar_clicked(self,boton): 
			
		preguntar=ask_confirm()
			
		if (preguntar.run()):
			print("Apagamos")
			
	def on_window1_destroy(self,event):  #Cerrar la ventana principal
		Gtk.main_quit()
	
			
''' CODIGO PRINCIPAL '''

Gtk.init(sys.argv)

ventana_principal=main_window()

Gtk.main()  #entramos en el bucle general de eventos. En este bucle el programa esta esperando que se genere algun evento y lo agrega al bucle de eventos.
